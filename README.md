### Para iniciar o projeto, voce deve ter o [postgres](https://www.postgresql.org/)

```bash
psql -h <host> --username <username> -W
<digite_sua_senha>
create database lanchonete;
```

1. Baixar o codigo
```git
git clone https://bitbucket.org/LuizSalvador/lanchonete.git lanchonete
```

2. Compilar o projeto com maven
```maven
cd lanchonete
./mvnw clean install
```

3. Rodar o projeto usando maven com spring boot
```maven
./mvnw spring-boot:run
```
